package com.mtyblewski.programmingchallengekistler;

import com.mtyblewski.programmingchallengekistler.entity.Message;
import com.mtyblewski.programmingchallengekistler.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.nio.file.Files;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureWebTestClient
@AutoConfigureDataMongo
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InitFilesIntegrationTests {

    @Autowired
    WebTestClient webTestClient;

    @SpyBean
    private UserRepository repository;

    @BeforeEach
    void setUp() {
        repository.deleteAll().subscribe();
    }

    @Test
    public void example_file() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/programming-challenge-input.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=programming-challenge-input.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findAll())
                .assertNext(user -> assertThat(user).matches(u -> !u.getName().isEmpty()
                        && u.getSubscriptions().stream().noneMatch(String::isEmpty)
                        && u.getMessages().stream().map(Message::getContent).noneMatch(String::isEmpty)))
                .thenCancel()
                .verify();
    }

    @Test
    public void empty_file() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/empty_file.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=empty_file.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findAll()).verifyComplete();
    }

    @Test
    public void empty_msg() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/empty_msg.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=empty_msg.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findByName("Name R")).assertNext(user -> assertThat(user.getMessages()).isEmpty()).verifyComplete();
    }

    @Test
    public void extra_large_one_msg() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/extra_large_one_msg.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=extra_large_one_msg.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findByName("YBuzpJNrKZ"))
                .assertNext(user -> assertThat(user.getMessages()).hasSize(1)
                        .matches(messages -> messages.get(0).getContent().length() == 915))
                .verifyComplete();
    }

    @Test
    public void large_msg() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/large_msg.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=large_msg.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findAll())
                .assertNext(user -> assertThat(user).matches(u -> !u.getName().isEmpty()
                        && u.getSubscriptions().stream().noneMatch(String::isEmpty)
                        && u.getMessages().stream().map(Message::getContent).noneMatch(String::isEmpty)))
                .thenCancel()
                .verify();
    }

    @Test
    public void many_users_to_many_users() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/many_users_to_many_users.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=many_users_to_many_users.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findAll())
                .assertNext(user -> assertThat(user).matches(u -> !u.getName().isEmpty()
                        && u.getSubscriptions().stream().noneMatch(String::isEmpty)
                        && u.getMessages().stream().map(Message::getContent).noneMatch(String::isEmpty)))
                .thenCancel()
                .verify();
    }

    @Test
    public void many_users_to_one_user() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/many_users_to_one_user.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=many_users_to_one_user.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findAll())
                .assertNext(user -> assertThat(user).matches(u -> !u.getName().isEmpty()
                        && u.getSubscriptions().stream().noneMatch(String::isEmpty)
                        && u.getMessages().stream().map(Message::getContent).noneMatch(String::isEmpty)))
                .thenCancel()
                .verify();
    }

    @Test
    public void one_users_to_many_users() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/one_users_to_many_users.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=one_users_to_many_users.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findByName("Noah Norton"))
                .assertNext(user -> assertThat(user).matches(u -> !u.getName().isEmpty()
                        && u.getSubscriptions().size() == 5
                        && u.getMessages().stream().map(Message::getContent).noneMatch(String::isEmpty)))
                .verifyComplete();
    }

    @Test
    public void msg_bad_char_in_username() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/msg_bad_char_in_username.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=msg_bad_char_in_username.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().is5xxServerError();
    }

    @Test
    public void msg_multiple_colons() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/msg_multiple_colons.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=msg_multiple_colons.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void name_bad_char_L() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/name_bad_char_L.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=name_bad_char_L.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void no_msg() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/no_msg.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=no_msg.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void no_name_R() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/no_name_R.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=no_name_R.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().is5xxServerError();
    }

    @Test
    public void user_self_subscription() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/user_self_subscription.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=user_self_subscription.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findAll())
                .assertNext(user -> assertThat(user).matches(u -> u.getName().length() > 0
                        && u.getSubscriptions().stream().noneMatch(s -> s.isEmpty() && u.getName().equals(user.getName()))
                        && u.getMessages().stream().map(Message::getContent).noneMatch(String::isEmpty)));
    }

    @Test
    public void windows_line_endings() throws IOException {
        var bodyBuilder = new MultipartBodyBuilder();
        bodyBuilder.part("file", Files.readAllBytes(new ClassPathResource("init-files/windows_line_endings.txt").getFile().toPath())).header("Content-Disposition", "orm-data; name=file; filename=windows_line_endings.txt");

        webTestClient.post()
                .uri("/readInitFile")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                .exchange()
                .expectStatus().isOk();

        StepVerifier.create(repository.findAll())
                .assertNext(user -> assertThat(user).matches(u -> !u.getName().isEmpty()
                        && u.getSubscriptions().stream().noneMatch(String::isEmpty)
                        && u.getMessages().stream().map(Message::getContent).noneMatch(String::isEmpty)))
                .thenCancel()
                .verify();
    }
}
