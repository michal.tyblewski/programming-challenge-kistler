package com.mtyblewski.programmingchallengekistler.service;

import com.mtyblewski.programmingchallengekistler.entity.Message;
import com.mtyblewski.programmingchallengekistler.repository.UserRepository;
import io.netty.util.internal.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class MessageService {

    private final UserRepository userRepository;

    public Mono<String> processMessage(String userA, String message) {
        if (StringUtil.isNullOrEmpty(message)) return Mono.empty();

        return userRepository.findByName(userA)
                .switchIfEmpty(Mono.error(new HttpClientErrorException(HttpStatus.NOT_FOUND, "User not found! for nick: " + userA)))
                .doOnError(throwable -> System.out.println(throwable.getMessage()))
                .flatMap(user -> {
                    user.getMessages().add(new Message(message));
                    return userRepository.save(user);
                })
                .map(user -> "User: %s now has: %d".formatted(user.getName(), user.getMessages().size()));
    }
}
