package com.mtyblewski.programmingchallengekistler.service;

import com.mtyblewski.programmingchallengekistler.entity.Message;
import com.mtyblewski.programmingchallengekistler.entity.User;
import com.mtyblewski.programmingchallengekistler.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private static User subscribeUserBByUserA(User user, User user2) {
        if (user.getSubscriptions().contains(user2.getName())) {
            return user;
        }

        user.getSubscriptions().add(user2.getName());
        return user;
    }

    public Mono<String> processRelationship(String userA, String userB) {
        if (userB.equals(userA)) {
            return Mono.just("User cannot subscribe to own account");
        }

        if (userB.isEmpty() || userA.isEmpty()) {
            return Mono.error(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "User must have name"));
        }

        return Mono.zip(
                        userRepository.findByName(userA).switchIfEmpty(userRepository.save(new User(userA))),
                        userRepository.findByName(userB).switchIfEmpty(userRepository.save(new User(userB))),
                        UserService::subscribeUserBByUserA)
                .flatMap(userRepository::save)
                .map(user -> "Processing relationship: %s subscribe to: %s. %s subscribes: %s".formatted(user.getName(), userB, user.getName(), user.getSubscriptions().size()));
    }

    public Flux<User> getUsers() {
        return userRepository.findAll();
    }

    public Flux<Message> getMassages(String name) {
        return userRepository.findByName(name).flatMapIterable(User::getMessages);
    }

    public Mono<Map<String, List<Message>>> getSubMsg(String username) {
        return userRepository.findByName(username)
                .flatMapIterable(User::getSubscriptions)
                .flatMap(userRepository::findByName)
                .collectMap(User::getName, User::getMessages);
    }

    public Mono<Message> postMsg(String username, String msg) {
        return userRepository.findByName(username)
                .flatMap(user -> {
                    var msgDocument = new Message(msg);
                    user.getMessages().add(msgDocument);
                    return userRepository.save(user);
                }).map(user -> user.getMessages().get(user.getMessages().size() - 1));
    }
}
