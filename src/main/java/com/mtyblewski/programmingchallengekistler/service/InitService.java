package com.mtyblewski.programmingchallengekistler.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class InitService {

    private final MessageService messageService;
    private final UserService userService;
    private static final Pattern initFilePattern = Pattern.compile("^(?<firstPart>.*?)\\W*(?<separator>[>:])\\W*(?<secondPart>.*)$");

    public Mono<String> processLine(String line) {
        var matcher = initFilePattern.matcher(line);
        if (matcher.find()) {
            String firstPart = matcher.group("firstPart");
            String separator = matcher.group("separator");
            String secondPart = matcher.group("secondPart");

            return switch (separator) {
                case ">" -> userService.processRelationship(firstPart, secondPart);
                case ":" -> messageService.processMessage(firstPart, secondPart);
                default -> Mono.error(new IllegalStateException("Unexpected value: " + separator));
            };
        } else {
            return Mono.empty();
        }
    }
}
