package com.mtyblewski.programmingchallengekistler.controller;

import com.mtyblewski.programmingchallengekistler.entity.Message;
import com.mtyblewski.programmingchallengekistler.entity.User;
import com.mtyblewski.programmingchallengekistler.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/users")
    public Flux<User> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/user/{name}/get-messages")
    public Flux<Message> getUserMessages(@PathVariable String name) {
        return userService.getMassages(name);
    }

    @GetMapping(value = "/user/get-sub-messages")
    public Mono<Map<String, List<Message>>> getMySubscriptionMsg(@RequestHeader("username") String username) {
        return userService.getSubMsg(username);
    }

    @PostMapping(value = "/user/post-messages")
    public Mono<Message> postMessage(@RequestHeader("username") String username, @RequestBody String msg) {
        return userService.postMsg(username, msg);
    }

}
