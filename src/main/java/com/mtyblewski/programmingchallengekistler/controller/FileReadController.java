package com.mtyblewski.programmingchallengekistler.controller;

import com.mtyblewski.programmingchallengekistler.service.InitService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

@RestController
@RequiredArgsConstructor
public class FileReadController {

    private final InitService initService;

    @PostMapping(path = "/readInitFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public Mono<String> readFile(@RequestPart("file") FilePart filePart) {
        return filePart.content()
                .flatMap(dataBuffer -> Flux.fromIterable(dataBuffer::readableByteBuffers))
                .flatMap(byteBuffer -> Flux.fromStream(StandardCharsets.UTF_8.decode(byteBuffer).toString().lines()))
                .concatMap(initService::processLine)
                .then(Mono.just("File processed successfully."));
    }
}
