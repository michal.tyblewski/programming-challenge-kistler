package com.mtyblewski.programmingchallengekistler.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import java.time.LocalDateTime;

@Data
public class Message {

    private String content;

    @CreatedDate
    private LocalDateTime created = LocalDateTime.now();

    public Message(String content) {
        this.content = content;
    }
}
