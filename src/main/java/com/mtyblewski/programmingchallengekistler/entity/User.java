package com.mtyblewski.programmingchallengekistler.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@Document
public class User {

    @Id
    private String id;

    @Indexed(unique = true)
    private String name;

    private List<String> subscriptions = new ArrayList<>();

    private List<Message> messages = new ArrayList<>();

    public User(String name) {
        this.name = name;

    }
}
