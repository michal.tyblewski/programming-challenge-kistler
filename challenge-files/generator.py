#!/usr/bin/env python3

import random
import string
import argparse
import os

def main(num_users, num_relationships, num_messages, min_msg_len, max_msg_len, line_endings):

    # Generate user names
    users = [''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase, k=10)) for _ in range(num_users)]

    # Generate user relationships
    relationships = [f"{random.choice(users)} > {random.choice(users)}{line_endings}" for _ in range(num_relationships)]

    # Generate user messages
    messages = [f"{random.choice(users)}: {' '.join([''.join(random.choices(string.ascii_lowercase, k=random.randint(1, 10))) for _ in range(random.randint(min_msg_len, max_msg_len))])}.{line_endings}" for _ in range(num_messages)]

    # Concatenate relationships and messages
    output_data = relationships + [f"{line_endings}"] + messages

    # Write data to a file with the appropriate line endings
    file_path = 'generated-input.txt'
    with open(file_path, 'w', newline='') as f:
        f.writelines(output_data)

    print(f"File saved with line endings {repr(line_endings)} at '{os.path.abspath(file_path)}'.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate large test file for user relationships and messages.')
    parser.add_argument('-u', '--num_users', type=int, default=1000, help='Number of users')
    parser.add_argument('-r', '--num_relationships', type=int, default=5000, help='Number of relationships')
    parser.add_argument('-m', '--num_messages', type=int, default=10000, help='Number of messages')
    parser.add_argument('-min', '--min_msg_len', type=int, default=5, help='Minimum length of a message')
    parser.add_argument('-max', '--max_msg_len', type=int, default=15, help='Maximum length of a message')
    parser.add_argument('-e', '--line_endings', choices=['linux', 'windows'], default='linux', help='Line endings style (linux or windows)')

    args = parser.parse_args()

    line_endings = '\n' if args.line_endings == 'linux' else '\r\n'

    main(args.num_users, args.num_relationships, args.num_messages, args.min_msg_len, args.max_msg_len, line_endings)
